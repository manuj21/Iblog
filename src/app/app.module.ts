import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { EditBlogPage } from '../pages/edit-blog/edit-blog';
import { BlogsPage } from '../pages/blogs/blogs';
import { BlogPage } from '../pages/blog/blog';
import { HttpModule } from '@angular/http';
import { AuthService } from '../services/auth';
import { BlogService } from '../services/blog';
import { ProfilePage } from '../pages/profile/profile';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    EditBlogPage,
    BlogsPage,
    BlogPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    EditBlogPage,
    BlogPage,
    BlogsPage,
    ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    BlogService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
