import { Component, ViewChild, OnInit } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { BlogsPage } from '../pages/blogs/blogs';
import { RegisterPage } from '../pages/register/register';
import { EditBlogPage } from '../pages/edit-blog/edit-blog';
import { ProfilePage } from '../pages/profile/profile';
import { AuthService } from '../services/auth';
@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
@ViewChild('nav') nav:NavController;
user:any;
  rootPage = HomePage;
  editBlogPage = EditBlogPage;
  homePage = BlogsPage;
  profilePage = ProfilePage;

  constructor(platform: Platform, 
  statusBar: StatusBar, 
  splashScreen: SplashScreen,
  private menuCtrl:MenuController,
private authService:AuthService) 
  {
    platform.ready().then(() => {
    statusBar.styleDefault();
    splashScreen.hide();
    });
  }
  ngOnInit(){
    this.authService.user.subscribe((user) => {
      this.user = user;
    });

  }


  onPageLoad(page: any) {
    if (page == this.editBlogPage || page == this.profilePage) {
      if (page == this.profilePage)
        this.nav.push(page);
        else
        this.nav.push(page, { mode: 'New' }).then(decision => {
          if (!decision)
            this.nav.setRoot(this.rootPage);
        });
      this.nav.setRoot(page, { mode: 'New' });


      this.menuCtrl.close();
      return;
    }
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }
  
    onLogout() {
      window.localStorage.clear();
      this.nav.setRoot(HomePage);
      this.menuCtrl.close();
    }
}

