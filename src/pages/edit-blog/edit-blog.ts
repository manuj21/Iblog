import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, App } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BlogService } from '../../services/blog';
import { BlogsPage } from '../blogs/blogs';
import { BlogPage } from '../blog/blog';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-edit-blog',
  templateUrl: 'edit-blog.html',
})
export class EditBlogPage implements OnInit{


  pageTitle = "";
  pageMode: string;
  blogForm: FormGroup;
  blog: any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private blogService:BlogService,
    public toast:ToastController,
    public app:App) {
  }

  ngOnInit() {
    this.pageMode = this.navParams.get('mode');
    this.blog = this.navParams.get('blog');
    this.pageTitle = this.pageMode + " Blog";

    if (this.pageMode == "Edit") {
      this.initForm(this.blog);
      return;
    }
    this.blog = {
      title: null,
      image: null,
      desc: null,
    };
    this.initForm(this.blog);
  }
  
  onSubmit() {
    if (this.pageMode == 'New')
      this.addNewBlog();
    else
      this.updateBlog();

  }
 
  initForm(data: any) {
    this.blogForm = new FormGroup({
      title: new FormControl(data.title, Validators.required),
      image: new FormControl(data.image, Validators.required),
      desc: new FormControl(data.desc, Validators.required)
    });
  }
 
  addNewBlog() {
    this.blogService.addBlog(this.blogForm.value)
      .subscribe(data => {
        if (data.success) {
          this.app.getRootNav().setRoot(BlogsPage);
          this.blogForm.reset();
        }
        this.toast.create({
          message: data.msg,
          duration: 2000
        }).present();

      });
  }

  updateBlog() {
    this.blogService.updateBlog(this.blogForm.value, this.blog._id)
      .subscribe((data) => {
        if (data.success) {
          this.app.getRootNav().setRoot(BlogsPage);
        }
        this.toast.create({
          message: data.msg,
          duration: 2000
        }).present();
      });


  }


  onCancel() {
    if (this.pageMode == "Edit")
    this.navCtrl.pop();
    else
    this.navCtrl.popToRoot();    
  }
} 