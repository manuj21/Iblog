import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, App } from 'ionic-angular';
import { BlogService } from '../../services/blog';
import { EditBlogPage } from '../edit-blog/edit-blog';
import { BlogsPage } from '../blogs/blogs';

@Component({
  selector: 'page-blog',
  templateUrl: 'blog.html',
})
export class BlogPage implements OnInit {

  user: any;
  blog:any;
  isLiked:boolean=false;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private alertCtrl:AlertController,
    private toast:ToastController,
    public blogService:BlogService,
    public app:App) {
  }


ngOnInit(){
  this.blog = this.navParams.get('blog');
  this.user = JSON.parse(window.localStorage.getItem('user'));
  let index = JSON.parse(window.localStorage.getItem('user')).liked.indexOf(this.blog._id);
  if (index > -1)
    this.isLiked = true;

}


onEditBlog() {
  this.navCtrl.push(EditBlogPage, { mode: 'Edit', blog : this.blog });
}

deleteBlog() {
  this.alertCtrl.create({
    title: "Confirm Delete ?",
    message: "Do you want to delete this blog",
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
        }
      },
      {
        text: 'Delete',
        handler: () => {
          this.blogService.deleteBlog(this.blog._id)
            .subscribe((data) => {
              if (data.success)
                this.app.getRootNav().setRoot(BlogsPage);

              this.toast.create({
                message: data.msg,
                duration: 3000
              }).present();
            });
        }
      }
    ]
  }).present();
}


onLike() {
  this.blogService.onLike(this.blog._id)
    .subscribe((data) => {

      if (data.success) {
        this.isLiked = true;
        let user = JSON.parse(window.localStorage.getItem('user'));
        user.liked.push(this.blog._id);
        window.localStorage.setItem('user', JSON.stringify(user));

      }
      else {
        this.toast.create({
          message: data.msg,
          duration: 3000
        }).present();
      }
    });
}

onComment() {
  this.alertCtrl.create({
    title: 'Add Comment',
    inputs: [
      {
        name: 'content',
        placeholder: 'Comment'
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
        }
      },
      {
        text: 'Save',
        handler: data => {
          this.blogService.onComment(this.blog._id, data)
            .subscribe((data: any) => {
              if (data.success) {
                this.blog.comments.unshift(data.comment);
              }
              else {
                this.toast.create({
                  message: data.msg,
                  duration: 3000
                }).present();
              }
            });
        }
      }
    ]
  }).present();
}


onDeleteComment(id: string) {
  this.alertCtrl.create({
    title: "Delete Comment",
    message: "Do you really want to delete this comment ?",
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {

        }
      },
      {
        text: "Delete",
        handler: () => {
          this.blogService.onDeleteComment(id)
            .subscribe(data => {
              if (data.success) {
                let index = this.blog.comments.findIndex(el => el._id == id);
                this.blog.comments.splice(index, 1);
              }
              this.toast.create({
                message: data.msg,
                duration: 3000
              }).present();
            });
        }
      }
    ]
  }).present();
}
}
