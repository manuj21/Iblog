import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage} from '../login/login';
import { AuthService } from '../../services/auth';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  constructor(private authService: AuthService,
    private navCtrl: NavController) { }

  onSignUp(form: NgForm) {
    let cssClass = "danger-class";
    this.authService.register(form.value)
      .subscribe((data) => {
        if (data.success) {
          this.navCtrl.setRoot(LoginPage);
          cssClass = "success-class";
          form.reset();
        }
      });
  }
}
