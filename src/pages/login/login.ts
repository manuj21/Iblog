import { Component } from '@angular/core';
import { NavController,ToastController } from 'ionic-angular';
import { BlogsPage } from '../blogs/blogs';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(private authService:AuthService,
    private navCtrl: NavController,
    private toastCtrl:ToastController) { }


  onLogin(form:NgForm) {
    this.authService.login(form.value)
      .subscribe((data) => {
        if (data.success) {
          window.localStorage.setItem('user', JSON.stringify(data.user));
          window.localStorage.setItem('auth-token', data.token);
          this.navCtrl.setRoot(BlogsPage);
          form.reset();
        }
        this.toastCtrl.create({
          message: data.msg,
          duration: 1200
        }).present();
      });

  }

}
