import { Component, OnInit } from '@angular/core';
import { NavController,ModalController, App } from 'ionic-angular';
import { BlogService } from '../../services/blog';
import { LoginPage } from '../login/login';
import { BlogPage } from '../blog/blog';
import { AuthService } from '../../services/auth';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-blogs',
  templateUrl: 'blogs.html',
})
export class BlogsPage implements OnInit {
  blogs : any[] =[];
  constructor(private navCtrl : NavController,
    public blogService : BlogService,
    public authService:AuthService,
    private app:App
    ) 
  { }


//All Blogs
  ngOnInit() {
    this.blogService.getAllBlogs()
    .subscribe((blogs:any[])=>{
      this.blogs=blogs;
    });
    var user;
    if (window.localStorage.getItem('user')) {
      user = JSON.parse(window.localStorage.getItem('user'));
    }
    this.authService.user.next(user);
  }

//OnViewBlog  
  onViewBlog(blog:any){
    this.navCtrl.push(BlogPage, { blog: blog }).then(decision => {
      if (!decision)
        this.app.getRootNav().setRoot(HomePage);
    });
  }
}
