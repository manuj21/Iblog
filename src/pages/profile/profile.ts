import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {

  user:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(){

    if (window.localStorage.getItem('user'))
      this.user = JSON.parse(window.localStorage.getItem('user'));
  }

}
