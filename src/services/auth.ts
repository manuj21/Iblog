import { Http, Headers, Response } from "@angular/http";
import 'rxjs/add/operator/map';
import { Injectable } from "@angular/core";
import { LoadingController } from "ionic-angular";
import { Subject } from "rxjs/Subject";
@Injectable()
export class AuthService {
    apiUrl: string = "http://localhost:3000/";
    user = new Subject<any>();
    constructor(private http: Http, private loadingCtrl: LoadingController) { }

    register(data: any) {
        let loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        loader.present();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.apiUrl + 'register', data, { headers: headers })
            .map((res: Response) => {
                loader.dismiss();
                return res.json();
            });
    }



    login(data: any) {
       
        
        let loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        loader.present();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.apiUrl + "login", data, { headers: headers })
            .map((res: Response) => {
                loader.dismiss();
                return res.json();
            });
    }
}