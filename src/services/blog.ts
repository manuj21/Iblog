import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/add/operator/map';
import { LoadingController } from "ionic-angular";
@Injectable()
export class BlogService {
    blogs: any[] = [];
    apiUrl: string = "http://localhost:3000/";

    constructor(private http: Http, private loadingCtrl: LoadingController) { 

    }
    

    header() {
        let token = window.localStorage.getItem('auth-token');
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', token);
        return headers;
    }


    loader(msg: string) {
        let loader = this.loadingCtrl.create({
            content: msg,
            duration: 10000
        });
        return loader;
    }

    getAllBlogs() {
        let loader = this.loader("Loading Blogs..");
        loader.present();
        return this.http.get(this.apiUrl + 'articles')
            .map((res: Response) => {
                this.blogs = res.json();
                loader.dismiss();
                return res.json();
            });
    }


    addBlog(blog: any) {
        let loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 10000
        });
        loader.present();

        let token = window.localStorage.getItem('auth-token');
        let headers = new Headers();

        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', token);

        return this.http.post(this.apiUrl + "articles", blog, { headers: headers })
            .map((res: Response) => {
                loader.dismiss();
                return res.json();
            });
    }

 
    updateBlog(data: any, id: string) {
        let loader = this.loader("Updating please wait...");
        loader.present();

        let headers = this.header();

        return this.http.put(this.apiUrl + 'articles/' + id, data, { headers })
            .map((res: Response) => {
                loader.dismiss();
                return res.json();
            });

    }
    
    deleteBlog(id: string) {
        let loader = this.loader("Deleting please wait...");
        loader.present();
        let headers = this.header();
        return this.http.delete(this.apiUrl + 'articles/' + id, { headers: headers })
            .map((res: Response) => {
                loader.dismiss();
                return res.json();
            })
    }

    
    onLike(id: string) {
        let token = window.localStorage.getItem('auth-token');
        let headers = new Headers();
        headers.append('Authorization', token);

        return this.http.get(this.apiUrl + "articles/" + id + "/like", { headers: headers })
            .map((res: Response) => {
                return res.json();
            });

    }
 
    onComment(id: string, comment: any) {

        let loader = this.loader("Adding Comment...");
        loader.present();

        let headers = this.header();
        return this.http.post(this.apiUrl + "articles/" + id + "/comment", comment, { headers: headers })
            .map((res: Response) => {
                loader.dismiss();
                return res.json();
            });

    }

    
    onDeleteComment(id: string) {

        let loader = this.loader("Deleting Comment...");
        loader.present();
        let headers = this.header();

        return this.http.delete(this.apiUrl + "comment/" + id, { headers: headers })
            .map((res: Response) => {
                loader.dismiss();
                return res.json();
            });

    }

}